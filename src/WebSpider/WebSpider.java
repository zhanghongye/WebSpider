
package WebSpider;

import java.io.BufferedReader;
import java.io.FileOutputStream;
//import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WebSpider {

    public static void main(String[] args) {       
        String url_str = "http://www.baidu.com/s?wd=%E5%B9%BF%E5%A4%96&ie=utf-8";
        URL url = null;
        String htm_str = null;
        try {
            url = new URL(url_str);
        } 
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
        
        String charset = "utf-8";
        int sec_cont = 1000;
        try {
            URLConnection url_con = url.openConnection();
            url_con.setDoOutput(true);
            url_con.setReadTimeout(10 * sec_cont);
            url_con.setRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)");
            InputStream htm_in = url_con.getInputStream();
            
            htm_str = InputStream2String(htm_in,charset);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
        Pattern pat = Pattern.compile("<a href=\"([^\"]*?)\"\\s+?target=\"_blank\">");
        Matcher mat = pat.matcher(htm_str);
        while(mat.find())
        	System.out.println(mat.group(1));
    }
    
    public static void saveHtml(String filepath, String str){
        
        try {
            OutputStreamWriter outs = new OutputStreamWriter(new FileOutputStream(filepath, true), "utf-8");
            outs.write(str);
            System.out.print(str);
            outs.close();
        } 
        catch (IOException e) {
            System.out.println("Error at save html...");
            e.printStackTrace();
        }
    }

    public static String InputStream2String(InputStream in_st,String charset) throws IOException{
        BufferedReader buff = new BufferedReader(new InputStreamReader(in_st, charset));
        StringBuffer res = new StringBuffer();
        String line = "";
        while((line = buff.readLine()) != null){
            res.append(line);
        }
        return res.toString();
    }

}
